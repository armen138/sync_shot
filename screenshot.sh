#!/bin/bash
# This screenshot program was based on an initial command-line only version
# by the purismforum user in community/librem-5
#
# I've since expanded it to use libnotify and provide a GUI
# It requires the grim, libnotify-bin and yad packages:
# sudo apt install grim yad libnotify-bin
#
# above notes by tor.sh from the pinephone matrix channel
# minor modifications to tie in with sync script by me (Armen138)
#
# Note to get requirements on arch:
# pacman -Syu grim yad libnotify

mkdir -p $HOME/Screenshots
SCREENSHOT="$HOME/Screenshots/$(date +%Y-%m-%d-%H%M%S).png"
NEXTCLOUD_SERVER="cloud.armen138.com"
NEXTCLOUD_USER="armen"

if [ -e /usr/bin/yad ]; then
  INPUT=`yad --title Screenshot --text="Take screenshot after X seconds" --form --field=filename:SFL --field=seconds:NUM "$SCREENSHOT" "5" --focus-field=2`
  echo $INPUT
  SCREENSHOT=`echo $INPUT | cut -d '|' -f1`
  SECONDS=`echo $INPUT | cut -d '|' -f2`
else
  SECONDS=`zenity --title Screenshot --text="Take screenshot after X seconds" --entry-text=5 --entry`
fi

if [ "$SECONDS" -eq 0 ]; then
  exit
fi
notify-send -t 1000 screenshot "Taking a screenshot in $SECONDS seconds"
sleep $SECONDS;
grim "$SCREENSHOT"

curl -n -T "$SCREENSHOT" https://$NEXTCLOUD_SERVER/remote.php/dav/files/$NEXTCLOUD_USER/Screenshots/

notify-send screenshot "Screenshot stored at ${SCREENSHOT}"
