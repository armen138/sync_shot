# This script uses .netrc for upload credentials
# see https://everything.curl.dev/usingcurl/netrc

# modify the following 4 lines to match your requirements
NEXTCLOUD_SERVER="cloud.armen138.com"
NEXTCLOUD_USER="armen"
LOCAL_PHOTO_PATH="$HOME/Pictures"
LOGFILE="$HOME/.photo_sync.log"

uploaded=0
touch $LOGFILE

function sync_photo() {
    if ! grep -Fxq "$1" $LOGFILE; then
        curl -s -n -T "$1" https://$NEXTCLOUD_SERVER/remote.php/dav/files/$NEXTCLOUD_USER/Photos/
        echo "$1" >> $LOGFILE
        uploaded=$((uploaded+1))
    fi
}

old_path=$PWD
cd $LOCAL_PHOTO_PATH
for i in *.png *.jpg *.JPG;
do
    sync_photo "$i"
done
echo "$uploaded photos uploaded to cloud."
cd $old_path