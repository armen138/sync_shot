# SyncShot

This is a couple of bash scripts to help backing up pictures shot on pinephone to a nextcloud instance.
This will also add an "app" icon to take screenshots with delay, and backs them up to nextcloud as well.

## Installation

Run install.sh


These scripts use .netrc for upload credentials
see https://everything.curl.dev/usingcurl/netrc

Aside from setting up your nextcloud credentials in a .netrc file, you also need to modify the sync.sh script:

```
NEXTCLOUD_SERVER="cloud.armen138.com"
NEXTCLOUD_USER="armen"
```

## Screenshot script

The following is taken from the screenshot.sh script:

> This screenshot program was based on an initial command-line only version
> by the purismforum user in community/librem-5
>
> I've since expanded it to use libnotify and provide a GUI
> It requires the grim, libnotify-bin and yad packages:
> sudo apt install grim yad libnotify-bin
>
> above notes by tor.sh from the pinephone matrix channel
> minor modifications to tie in with sync script by me (Armen138)
>
> Note to get requirements on arch:
> pacman -Syu grim yad libnotify
